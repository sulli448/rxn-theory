#!/bin/bash

dens < inputfile_ox_dens_c12
mv dens.dao dens.dao_c12
dens < inputfile_ox_dens_n12
mv dens.dao dens.dao_n12

shell < inputfile_ox_shell_lpe_c12
./c12.bat
shell < inputfile_ox_shell_lpe_n12
./n12.bat
shell < inputfile_ox_shell_den
./c12c12.bat
